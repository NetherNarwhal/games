# Platformer
`Platformer.html` is a simple platformer style game based on a version created by Jake Gordon ([Source code](https://github.com/jakesgordon/javascript-tiny-platformer), [Basic Article](http://codeincomplete.com/posts/2013/5/27/tiny_platformer/), [Follow up article adding monsters and treasures](http://codeincomplete.com/posts/2013/6/2/tiny_platformer_revisited/)). It does not require any other libraries or files. Once downloaded, just click (or double click) on it to load it into your browser and start playing. I have cleaned it up some, merged files, and brought it more in line with ES6 as well as added additional features.

## Learning Exercises
Also included in this directory is version of the game called `class.html`, which does not implement all features. This provides a consistent starting point for folks looking to implement things themselves. The following are a couple good exercises for those new to Javascript to get started on. These are listed by increasing difficultly. *Not all are currently implemented in the `platformer.html` version.*

* There is lava at the bottom of the level, but it does no damage when you walk on it. Change the lava so that it "kills" the player and resets them back to their starting spot, the same way touching a monster would do.
* Add a different type of treasure, such as a red ruby or blue diamond that is a circle, triangle, or other shape. [Drawing Shapes](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes) is a good guide for how to draw these.
* Add a power up, such as temporary invincibility, the ability to double jump or wall jump.
* Add a switch or key to unlock other unreachable parts of the level.
* Add moveable platforms.
* Add a different type of monster that flies, traverses vertical perhaps all the time or only with the player is near (ex. [Thwomp](https://www.mariowiki.com/Thwomp) in the Mario games).
* Add an exit and other levels.

### Solutions/Spoliers
<details>
<summary>Add Lava Damage</summary>

There is already logic to check which blocks the player is touching, so just need to update them to account for lava's deadliness. Add the following code to `updateEntity()` at the bottom of the first if check (`if ((celldown && !cell) || (celldiag && !cellright && nx)) {`):

```javascript
if (celldown && TILES[celldown].deadly) {
    if (entity.isPlayer) killPlayer(entity);
    else if (entity.isMonster) killMonster(entity);
}
```
</details>

<details>
<summary>Add Diamond Treasure</summary>

Let's add a triangular shaped diamond treasure.

### Level Data
First, replace one of the `o`'s on the map (in `levelData` constant) with a `d` to denote a diamond. I chose the one near the top middle, but any will do.

Now that the map contains a diamond, let's add support to the logic loading the level to support the new diamond type. Add a new condition in the `switch` within the `setup` method to support `d`, like this:
```javascript
case "d": treasure.push(new Entity({ x, y, type:"treasure", treasureType: "diamond" })); break;
```

This logic is pretty much the same as the logic for loading the gold pieces, however we have added a `treasureType` property. We now need to add that new property to our `Entity` class to tell the difference between the regular treasure (gold) and a diamond. Add the following code in the `Entity` constructor:
```javascript
this.treasureType = props.treasureType;
```

### Rendering
First, add a new color, `BLUE: '#66C'`, to the `COLORS` constant.

Now we need to rework the renderTreasure method to support drawing the diamond. The new one should look like this:
```javascript
function renderTreasure(ctx, frame) {
    ctx.globalAlpha = 0.25 + tweenTreasure(frame, 60);
    for (let n = 0, max = treasure.length; n < max; n++) {
        const t = treasure[n];
        if (t.collected) continue;
        if (t.treasureType === "diamond") {
            ctx.fillStyle = COLOR.BLUE;
            ctx.beginPath();
            ctx.moveTo(t.x, t.y + diamondOffset);
            ctx.lineTo(t.x + TILE, t.y + diamondOffset);
            ctx.lineTo(t.x + TILE_HALF, t.y + TILE - diamondOffset);
            ctx.fill();
        } else { // Gold, by default.
            ctx.fillStyle = COLOR.GOLD;
            ctx.fillRect(t.x, t.y + TILE / 3, TILE, TILE * 2 / 3);
        }
    }
    ctx.globalAlpha = 1;
}
```

You should now have a triangular blue diamond in place of one of you treasures. It should pulse just like the other treasures. For a future improvement you could change the render logic in `renderPlayer` method to show the diamond differently than the other treasure. Note that this would also means having a different counter than just `player.collected` in order to tell which is which though and then updating the `collectTreasure` method to set the right thing.
</details>

<details>
<summary>Add an Invincibility Power Up</summary>

This change will add an invincibility power up that will make the player impossible to kill for 5 seconds, or whatever duration you set. The first step is to create the power up itself, which is very similar to how treasures work.

### Power Up Level & Entity Data
We'll use the letter `*` to represent the invincibility power up. Pick a place on the map (in `levelData` constant) and enter a `*`. I chose the one near the middle, but any will do.

Now that the map contains a power up, let's add support to the logic loading the level to support the entity. Add a new condition in the `switch` within the `setup` method to support `*d*`, like this:
```javascript
case "*": powerUps.push(new Entity({ x, y, type:"powerup" })); break;
```
You'll note that we added a new variable to hold power-up entities, so define that at the top of the script along with the others via `let powerUps = [];`. We'll also need to add a new attribute to the entity class in the constructor, `this.isPowerUp = props.type == "powerup";`, to note that the entity is a power up.

### Power Up Rendering
Let's create a helper method to make drawing polygons, like a star, generic so we can change the shape easily in the future if we want to. This takes advantage of the fact that a polygon is just points on a circle at regular intervals/angles. For instance, a triangle is 3 points on the same circle with equal angle (360 / 3 or 120 degrees) between them connected by lines. Or put another way, pick a point at any angle, say 0 degrees, on a circle and start a line. Draw that line to the next point 120 degrees further along the circle, then the next point at 240 degrees, and finally draw the line to 360 degrees (same as 0) which takes us back to the start. Stars work the same way for the outer points, but in-between them are points on a smaller circle within. By alternating back and forth you get the points of the star.

Note that instead of degrees, the Javascript math functions take radians. One PI (`Math.PI`) radians is 180 degrees or half a circle, so a full circle, or 360 degrees, is `2 Math.PI` radians. 

Here is the full method to draw either a regular polygon, where are sides and angles are equal, or a star, which is a concave polygon with points.
```javascript
function drawPolygon(ctx, cx, cy, numPoints=5, radius, innerRadius, rotate=0, fill="#000", stroke) {
    const step = (innerRadius ? Math.PI : Math.PI * 2) / numPoints;
    ctx.beginPath();
    // Set first point. Optimize for rotation of 0 where we can skip the cos/sin calls, which can be expensive.
    if (rotate === 0) {
        ctx.moveTo(cx + radius, cy);
    } else {
        ctx.moveTo(cx + (Math.cos(rotate) * radius), cy + (Math.sin(rotate) * radius));
    }
    rotate += step;
    if (innerRadius) { // See if it is a star shape.
        for (let i = 0; i < numPoints; i++) {
            // Draw next inner point.
            ctx.lineTo(cx + (Math.cos(rotate) * innerRadius), cy + (Math.sin(rotate) * innerRadius));
            rotate += step;
            // Draw next outer point.
            ctx.lineTo(cx + (Math.cos(rotate) * radius), cy + (Math.sin(rotate) * radius));
            rotate += step;
        }
    } else {
        for (let i = 1; i < numPoints; i++) {
            ctx.lineTo(cx + (Math.cos(rotate) * radius), cy + (Math.sin(rotate) * radius));
            rotate += step;
        }
    }

    if (fill) {
        ctx.fillStyle = fill;
        ctx.fill();
    }
    if (stroke) { // Outline
        ctx.strokeStyle = stroke;
        ctx.stroke();
    }
}
```

Now we need to actually make it render. For this we'll need to add a render method, which will reuse the fading in and out of the treasures:
```javascript
function renderPowerUps(ctx, frame) {
    ctx.globalAlpha = 0.25 + tweenTreasure(frame, 60);
    const radius = TILE_HALF + TILE_HALF / 8;
    const innerRadius = TILE_HALF /2;
    for (const power of powerUps) {
        // x & y are the upper left corner of the tile. Need to add half tile to get the center of the shape.
        drawPolygon(ctx, power.x + TILE_HALF, power.y + TILE_HALF, 5, radius, innerRadius, -Math.PI / 2, COLOR.YELLOW);
    }
    ctx.globalAlpha = 1;
}
```

Then have the main render method call this via `renderPowerUps(ctx, frame);`.

### Power Up Effect
Now that we have power ups and can render them we need to handle what happens when the player actually picks one up. We first add a function to handle the collision detection and setting invincibility on the player when they touch the power up.

```javascript
function checkPowerUp() {
    // Count down instead of up because when a power up is removed (touched) we'll end up skipping the next power up checked.
    for (let i = powerUps.length - 1; i >= 0; i--) {
        const powerUp = powerUps[i];
        if (overlap(player.x, player.y, TILE, TILE, powerUp.x, powerUp.y, TILE, TILE)) {
            // Player has picked up the power up. Give them 10 seconds of invincibility. This will count down each frame.
            player.invincible = 10;
            powerUps.splice(i, 1); // Remove the power up from the list.
        }
    }
}
```

Add a counter attribute to the following to the Entity constructor: `this.invincible = 0;`. When this is set to a number greater than 0 then the player will become invincible until the value drops down to 0 or below again. We decrease, or decrement, this counter each frame in `updateEntity`.
```javascript
// Decrement counters.
if (entity.invincible > 0) entity.invincible -= dt;
```

Change the check in `updateMonster` to kill the monster instead of player when the player is invincible. We do this by adding an invincibility check as the first condition of this IF statement.
```javascript
if (player.invincible > 0 || ((player.dy > 0) && (monster.y - player.y > TILE_HALF))) {
    killMonster(monster);
} else {
    killPlayer(player);
}
```

Have the player flash colors in `renderPlayer` while they are invincible so the user knows when it ends. Replace setting the fillStyle to YELLOW with this.
```javascript
    if (player.invincible > 0) {
        // Make the player flash while invincible.
        const startColor = Number.parseInt(COLOR.YELLOW.substring(1), 16);
        const percentAlong = player.invincible / 10;
        ctx.fillStyle = "#" + Math.trunc(startColor + (0xffffff - startColor) * percentAlong).toString(16);
    } else {
        ctx.fillStyle = COLOR.YELLOW;
    }

```

</details>