This project is a collection of various JavaScript based games and their source code. These games are designed to run in the browser without the need of a web server in order to keep development fast and easy to learn, however this does come with some restrictions (ex. no modules or ability to load data files due to CORS).

The root directory contains shared files used by many games, including any third-party libraries. The individual games are in their own folder, typically with one html file per game.

*Note that these were primarily developed on a desktop, so mobile support is often lacking.*

## Installing
Unfortunately, you can't run many of these games from Gitlab itself. You'll need to download them via Git or as a compressed archive, like a ZIP file, following the instructions below. Once downloaded you should not need to run a web server or require any external network connectivity for any of the games unless otherwise noted. Follow either of the options below to get a copy of the files. Then you can run the game simply by double clicking the html files.

### Via Git
* Click on the blue clone button (near top of page on right side) and copy thr url.
* In a parent folder of where you'd like this project created type `git clone <url you copied above>`.
* You should now be able to double click on any of the *.html files in the project to run them.

### Via Zip File
* Click on the download button (to left of the blue clone button in right side of page) and select your format.
* Unzip the contents somewhere on your computer. Make sure it preserves the folder structure.
* You should now be able to double click on any of the *.html files in the project to run them.

## Third Party Libraries and Other Content
Third party Libraries:
* [Planck](https://github.com/shakiba/planck.js) ([MIT License](https://github.com/shakiba/planck.js/blob/master/LICENSE.txt)) - 2D physics engine.
* [Three.js](https://github.com/mrdoob/three.js) ([MIT License](https://github.com/mrdoob/three.js/blob/dev/LICENSE)) - 3D game engine (i.e. wrapper around WebGL).

Graphics:
* [BoxIcons](https://github.com/atisawd/boxicons) ([Demo](https://boxicons.com), [CC 4.0 License](https://creativecommons.org/licenses/by/4.0/)) - SVG icons with more of an application and business flair.
* [Game-Icons](https://game-icons.net/) ([CC BY 3.0](https://creativecommons.org/licenses/by/3.0/)) - SVG icons that are primarily game related.